# Mini Repros SNIN <sub><sup>(d'après une idée originale de Feel-My-Geek)</sup></sub>

## À propos
Les minis repros sont des petites boites pour embellir vos cartouches Super Nintendo, sur le principe des ["N" Mini's de Feel-My-geek](https://nminis.blogspot.com/).
![](resources/overview_credits.jpg)

Il s'agit pour la plupart de mes créations, réalisées en repartant du template initial de Feel-My-Geek. Les suivantes sont des créations de Feel que j'ai modifiées à la marge (en ajoutant le seal FMG, ou en modifiant la couverture):
- Dragon Ball Z 2
- Magical Quest
- Mr Nutz
- NBA Jam
- Super Mario World 2
- Super Street Fighter 2
- UN Squadron

## Contenu
- les jpg prêts à être imprimés,
- resources : les fichiers GIMP pour les repros de mon cru,
- templates : les fichiers GIMP vierges.

## Design
Il reprend le format classique des boîtes Super Nintendo. 
![](resources/DKC_front_credits.jpg)
![](resources/DKC_back_credits.jpg)

Juste une modif : l'une des tranches présente les titres dans leur version japonaise.
![](resources/side_credits.jpg)
![](resources/side_jp_credits.jpg)

## Crédits
Vous pouvez utiliser les images et les sources comme bon vous semble.

N'oubliez pas de citer vos sources : Feel-My-Geek et moi-même si vous utilisez mes créations.

Merci!